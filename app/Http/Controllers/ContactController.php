<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index()
    {
        $companyName = 'arome Saint germain';
        $companyPhone = '(780) 995-4604';
        return view('contact.index', compact('companyName', 'companyPhone'));
    }
}
